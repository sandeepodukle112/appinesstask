package com.demo.appinesstask.view.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.appinesstask.R;
import com.demo.appinesstask.databinding.ActivityMainBinding;
import com.demo.appinesstask.service.model.Task;
import com.demo.appinesstask.view.adapter.TaskAdapter;
import com.demo.appinesstask.viewmodel.TaskViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private ActivityMainBinding mBinding;
    private List<Task> taskList;
    private TaskAdapter taskAdapter;
    private TaskViewModel taskViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        init();
        setClickListeners();
        setAdapter();
        apiCall();
    }

    private void init() {
        taskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);
    }

    private void setClickListeners() {
        mBinding.imageViewSearch.setOnClickListener(this);
        mBinding.editTextSearch.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideKeyboard();
                search(mBinding.editTextSearch.getText().toString().trim());
            }
            return false;
        });
    }

    private void setAdapter() {
        taskList = new ArrayList<>();
        final int defaultPadding = getResources().getDimensionPixelOffset(R.dimen.padding_16);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mBinding.recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                int position = parent.getChildAdapterPosition(view);
                if (position == RecyclerView.NO_POSITION) {
                    return;
                }
                outRect.top = position == 0 ? defaultPadding / 2 : defaultPadding / 4;
                if (parent.getAdapter() != null)
                    outRect.bottom = position == parent.getAdapter().getItemCount() - 1 ? defaultPadding / 2 : defaultPadding / 4;
                outRect.left = defaultPadding / 2;
                outRect.right = defaultPadding / 2;
            }
        });
        taskAdapter = new TaskAdapter(taskList);
        mBinding.recyclerView.setAdapter(taskAdapter);
    }

    private void apiCall() {
        if (isOnline()) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
            taskViewModel.getTaskListObservable().observe(this, tasks -> {
                if (tasks.size() > 0) {
                    taskAdapter.addAll(tasks);
                    mBinding.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.textViewNoDataFound.setVisibility(View.GONE);
                } else {
                    mBinding.textViewNoDataFound.setVisibility(View.VISIBLE);
                    mBinding.recyclerView.setVisibility(View.GONE);
                }
                mBinding.progressBar.setVisibility(View.GONE);
            });
        } else {
            showDialog();
        }
    }

    private void search(String query) {
        List<Task> searchList = new ArrayList<>();
        query = query.toLowerCase();
        if (taskViewModel.getTaskListObservable().getValue() != null) {
            if (query.isEmpty()) {
                searchList.addAll(taskViewModel.getTaskListObservable().getValue());
            } else {
                for (Task task : taskViewModel.getTaskListObservable().getValue()) {
                    if (task.getTitle().toLowerCase().contains(query)) {
                        searchList.add(task);
                    }
                }
            }

            if (searchList.size() > 0) {
                taskAdapter.addAll(searchList);
                mBinding.recyclerView.setVisibility(View.VISIBLE);
                mBinding.textViewNoDataFound.setVisibility(View.GONE);
            } else {
                mBinding.textViewNoDataFound.setVisibility(View.VISIBLE);
                mBinding.recyclerView.setVisibility(View.GONE);
            }
        }
    }

    private void showDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Alert")
                .setMessage("Check network connection!")
                .setPositiveButton("Ok", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    finish();
                })
                .show();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.image_view_search) {
            if (mBinding.editTextSearch.getVisibility() == View.VISIBLE) {
                mBinding.editTextSearch.setVisibility(View.GONE);
                mBinding.imageViewSearch.setImageResource(R.drawable.ic_search_white);
                mBinding.editTextSearch.setText("");
                resetList();
                hideKeyboard();
            } else {
                mBinding.imageViewSearch.setImageResource(R.drawable.ic_cross);
                mBinding.editTextSearch.setVisibility(View.VISIBLE);
                mBinding.editTextSearch.requestFocus();
            }
        }
    }

    private void resetList() {
        taskList = taskViewModel.getTaskListObservable().getValue();
        taskAdapter.addAll(taskList);
        mBinding.textViewNoDataFound.setVisibility(View.GONE);
        mBinding.recyclerView.setVisibility(View.VISIBLE);
    }

    private void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            if (getCurrentFocus() != null)
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting() && netInfo.isAvailable() && netInfo.isConnected();
    }
}

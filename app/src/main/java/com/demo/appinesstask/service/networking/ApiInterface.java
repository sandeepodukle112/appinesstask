package com.demo.appinesstask.service.networking;

import com.demo.appinesstask.service.model.Task;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface ApiInterface {
    @GET("api.json")
    Observable<Response<List<Task>>> getDataFromApi();
}

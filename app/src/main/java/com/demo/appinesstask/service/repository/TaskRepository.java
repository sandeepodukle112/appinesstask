package com.demo.appinesstask.service.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.demo.appinesstask.service.model.Task;
import com.demo.appinesstask.service.networking.ApiInterface;
import com.demo.appinesstask.service.networking.RetrofitService;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class TaskRepository {
    private static final String TAG = "TaskRepository";

    private TaskRepository() {
    }

    public static TaskRepository getInstance() {
        return InstanceHelper.INSTANCE;
    }

    public LiveData<List<Task>> callApi() {
        final MutableLiveData<List<Task>> taskList = new MutableLiveData<>();
        Observable<Response<List<Task>>> observable;
        observable = RetrofitService.getInstance().builder().create(ApiInterface.class).getDataFromApi();
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<List<Task>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "onSubscribe: ");
                    }

                    @Override
                    public void onNext(Response<List<Task>> listResponse) {
                        Log.d(TAG, "onNext: ");
                        taskList.setValue(listResponse.body());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: " + e);
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: ");
                    }
                });
        return taskList;
    }

    private static class InstanceHelper {
        private static TaskRepository INSTANCE = new TaskRepository();
    }
}

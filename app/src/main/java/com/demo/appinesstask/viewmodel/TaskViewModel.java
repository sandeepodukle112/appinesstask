package com.demo.appinesstask.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.demo.appinesstask.service.model.Task;
import com.demo.appinesstask.service.repository.TaskRepository;

import java.util.List;

public class TaskViewModel extends AndroidViewModel {
    private LiveData<List<Task>> taskListObservable;

    public TaskViewModel(@NonNull Application application) {
        super(application);
        taskListObservable = TaskRepository.getInstance().callApi();
    }

    public LiveData<List<Task>> getTaskListObservable() {
        return taskListObservable;
    }
}
